import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-hello-world',
  templateUrl: './hello-world.component.html',
  styleUrls: ['./hello-world.component.scss']
})
export class HelloWorldComponent {

  serviceResult$?: Observable<any>;

  constructor(private httpClient: HttpClient) { }

  callTheService(): void {
    this.serviceResult$ = this.httpClient.get<any>(environment.serviceBaseUri + 'hello').pipe(map(o => o.info));
  }

}
