import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
  <nav class="navbar">
    <h1>{{title}}</h1>
    <div class="links"><a [routerLink]="['/hello']" class="link">Hello</a></div>
    
  </nav>
  <router-outlet></router-outlet>
`,
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'interview-ngui';
}
